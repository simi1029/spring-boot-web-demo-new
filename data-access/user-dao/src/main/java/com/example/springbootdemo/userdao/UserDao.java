package com.example.springbootdemo.userdao;

import java.util.List;
import java.util.Optional;

import com.example.springbootdemo.userdao.entity.UserEntity;

public interface UserDao {

    UserEntity save(UserEntity user);

    Optional<UserEntity> findById(Long id);

    List<UserEntity> findAll();

}
