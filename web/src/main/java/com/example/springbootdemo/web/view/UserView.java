package com.example.springbootdemo.web.view;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public record UserView(
        Long id,
        @NotBlank String name,
        @NotBlank @Email String email,
        String createdDate) {

}
