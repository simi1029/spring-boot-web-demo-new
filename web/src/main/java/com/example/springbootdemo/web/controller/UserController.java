package com.example.springbootdemo.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.springbootdemo.service.user.UserService;
import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.web.transformer.UserViewTransformer;
import com.example.springbootdemo.web.view.UserView;

@Controller
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private final UserViewTransformer userTransformer;
    private final UserService userService;

    public UserController(UserViewTransformer userTransformer, UserService userService) {
        this.userTransformer = userTransformer;
        this.userService = userService;
    }

    @GetMapping("/users")
    public ModelAndView users() {
        ModelAndView view = new ModelAndView("user-list");
        view.addObject("users", userTransformer.transform(userService.findAll()));
        return view;
    }

    @GetMapping("/signup")
    public String signUp(Model model, UserView userView) {
        model.addAttribute("user", userView);
        return "signup";
    }

    @PostMapping("/signup")
    public String signup(@Valid UserView user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOGGER.error("Error while saving users: {}", result.getFieldErrors());
            model.addAttribute("user", user);
            return "signup";
        }
        User savedUser = userService.save(userTransformer.transformView(user));
        return "redirect:/user/" + savedUser.id();
    }

    @GetMapping("/user/{id}")
    public ModelAndView userDetails(@PathVariable Long id) {
//        UserEntity user = userRepository.findById(id)
//                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found with id " + id));
        User user = userService.findById(id);
        ModelAndView userDetails = new ModelAndView("user-details");
        userDetails.addObject("user", userTransformer.transform(user));
        return userDetails;
    }
}
