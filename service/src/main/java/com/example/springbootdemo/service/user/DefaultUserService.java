package com.example.springbootdemo.service.user;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.service.user.domain.exception.UserNotFoundException;
import com.example.springbootdemo.service.user.transformer.UserTransformer;
import com.example.springbootdemo.userdao.UserDao;

@Service
public class DefaultUserService implements UserService {

    private final UserDao userDao;
    private final UserTransformer userTransformer;

    DefaultUserService(UserDao userDao, UserTransformer userTransformer) {
        this.userDao = userDao;
        this.userTransformer = userTransformer;
    }

    @Override
    public User save(User user) {
        return userTransformer.transform(userDao.save(userTransformer.transform(user)));
    }

    @Override
    public User findById(Long id) {
        return userDao.findById(id)
                .map(userTransformer::transform)
                .orElseThrow(() -> new UserNotFoundException("User not found with id " + id));
              //.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found with id " + id));
    }

    @Override
    public List<User> findAll() {
        return userTransformer.transform(userDao.findAll());
    }
}
