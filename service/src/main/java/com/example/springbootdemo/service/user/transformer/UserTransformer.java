package com.example.springbootdemo.service.user.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.springbootdemo.service.user.domain.User;
import com.example.springbootdemo.userdao.entity.UserEntity;

@Component
public class UserTransformer {

    public List<User> transform(List<UserEntity> userEntities) {
        return userEntities.stream()
                .map(this::transform)
                .collect(Collectors.toList());
    }

    public User transform(UserEntity entity) {
        return new User(entity.getId(), entity.getName(), entity.getEmail(), entity.getCreatedDate(), entity.getLastModified());
    }

    public UserEntity transform(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.id());
        userEntity.setName(user.name());
        userEntity.setEmail(user.email());
        userEntity.setCreatedDate(user.createdDate());
        userEntity.setLastModified(user.lastModified());
        return userEntity;
    }
}
